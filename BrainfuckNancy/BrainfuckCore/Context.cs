﻿namespace BrainfuckCore
{
    internal class Context
    {
        public readonly Memory Memory;
        public CommandSequence Commands { get; private set; }
        public InputStream Inputs { get; private set; }

        public Context(int memorySize)
        {
            this.Memory = new Memory(memorySize);
            this.Commands = CommandSequence.Default;
            this.Inputs = InputStream.Default;
        }

        /// <summary>
        /// 初期化します
        /// </summary>
        public void Init()
        {
            this.Memory.Init();
        }

        /// <summary>
        /// コマンドをセットします
        /// </summary>
        /// <param name="commands"></param>
        public void SetCommands(string commands)
        {
            this.Commands = new CommandSequence(commands);
        }

        /// <summary>
        /// 入力をセットします
        /// </summary>
        /// <param name="input"></param>
        public void SetInput(string input)
        {
            this.Inputs = new InputStream(input);
        }
    }
}
