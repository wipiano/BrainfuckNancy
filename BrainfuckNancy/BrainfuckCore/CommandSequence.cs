﻿using System.Linq;
using BrainfuckCore.Command;
using BrainfuckCore.Exception;

namespace BrainfuckCore
{
    /// <summary>
    /// Brainfuck コマンドの並び
    /// </summary>
    internal sealed class CommandSequence
    {
        /// <summary>
        /// 空のシーケンス
        /// </summary>
        public static readonly CommandSequence Default = new CommandSequence(string.Empty);

        private readonly Command.Command[] _commands;
        private uint _pointer;

        /// <summary>
        /// 現在のコマンド
        /// </summary>
        private Command.Command Current => _commands[_pointer];

        /// <summary>
        /// コマンドが終わりかどうか
        /// </summary>
        public bool IsEnd => _pointer >= _commands.Length;

        public CommandSequence(string code)
        {
            _pointer = 0;
            _commands = code.ToCommands().ToArray();
        }

        /// <summary>
        /// 現在のコマンドを実行し、次のコマンドに進みます。
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public char? ExecuteStep(Context context)
        {
            char? result = Current.Execute(context);
            _pointer++;
            return result;
        }

        /// <summary>
        /// 現在位置の Openコマンドに　対応する Closeコマンド(']')を検索して位置を返します
        /// </summary>
        /// <returns></returns>
        public uint SearchNextClose()
        {
            return SearchPair(SingletonCommands.Open, SingletonCommands.Close, true);
        }

        /// <summary>
        /// 現在位置の Close コマンドに対応する Openコマンドを検索して位置を返します
        /// </summary>
        /// <returns></returns>
        public uint SearchPreviousOpen()
        {
            return SearchPair(SingletonCommands.Close, SingletonCommands.Open, false);
        }

        /// <summary>
        /// ペアになるコマンドを、現在の位置以降で探して位置を返します
        /// </summary>
        /// <param name="that">自分自身</param>
        /// <param name="target">ペアになるコマンド</param>
        /// <param name="searchNext">true であれば現在位置より後ろを検索します。 false であれば現在位置より前を検索します</param>
        /// <returns></returns>
        private uint SearchPair(Command.Command that, Command.Command target, bool searchNext)
        {
            if (IsEnd)
            {
                throw new DestinationCloseNotFoundException();
            }

            var step = searchNext ? 1 : -1;
            long index = _pointer + step;
            int nest = 0;
            while (index < _commands.Length && index > 0)
            {
                if (_commands[index] == that)
                {
                    // 自分自身 が見つかったらネストのカウントを増やしておく
                    nest++;
                }
                else if (_commands[index] == target)
                {
                    if (nest > 0)
                    {
                        // ターゲットが見つかったらネストが一つ減る
                        nest--;
                    }
                    else
                    {
                        // ネストしてない場合はターゲットが見つかったらそれを返す
                        return (uint)index;
                    }
                }

                index += step;
            }

            throw new DestinationCloseNotFoundException();
        }

        /// <summary>
        /// 指定した位置のコマンドにジャンプします
        /// </summary>
        /// <param name="pointer"></param>
        public void Jump(uint pointer)
        {
            _pointer = pointer;
        }
    }
}
