﻿namespace BrainfuckCore.Exception
{
    public class DestinationOpenNotFoundException : BrainfuckException
    {
        private const string DefaultMessage = "] に対応する [ が見つかりませんでした。";

        internal DestinationOpenNotFoundException()
            : base(DefaultMessage)
        {
        }
    }
}
