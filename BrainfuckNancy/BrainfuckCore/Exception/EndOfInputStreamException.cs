﻿namespace BrainfuckCore.Exception
{
    public class EndOfInputStreamException : BrainfuckException
    {
        private const string DefaultMessage = "入力された範囲を超えて入力を読み取ろうとしました";

        internal EndOfInputStreamException()
            :base(DefaultMessage)
        {
        }
    }
}
