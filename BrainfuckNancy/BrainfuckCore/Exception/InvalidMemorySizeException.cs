﻿namespace BrainfuckCore.Exception
{
    public class InvalidMemorySizeException: BrainfuckException
    {
        internal InvalidMemorySizeException(int inputSize)
            : base($"不正なメモリサイズです。メモリサイズに{inputSize}が指定されました")
        {
        }
    }
}
