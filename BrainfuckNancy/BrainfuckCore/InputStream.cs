﻿using System.Linq;
using BrainfuckCore.Exception;

namespace BrainfuckCore
{
    internal sealed class InputStream
    {
        public static InputStream Default = new InputStream(string.Empty);

        private readonly byte[] _inputs;
        private uint _pointer;
        private bool IsEnd => _pointer >= _inputs.Length;
        private byte Current => _inputs[_pointer];

        public InputStream(string input)
        {
            _pointer = 0;
            _inputs = (input ?? string.Empty).ToCharArray().Select(c => (byte) c).ToArray();
        }

        /// <summary>
        /// 次の入力を読み取ります
        /// </summary>
        /// <returns></returns>
        public byte Read()
        {
            if (IsEnd)
            {
                throw new EndOfInputStreamException();
            }
            byte current = Current;
            _pointer++;
            return current;
        }
    }
}
