﻿using System.Collections.Generic;

namespace BrainfuckCore
{
    public class Interpreter
    {
        private readonly Context context;

        /// <summary>
        /// 与えられたコードをすぐに実行して結果を返します。
        /// 使用した Interpreter のインスタンスは破棄されます
        /// </summary>
        public static IEnumerable<char> ExecuteOnce(int memorySize, string code, string input)
        {
            return new Interpreter(memorySize).Load(code).Execute(input);
        }

        /// <summary>
        /// 新しい Brainfuck インタプリタを生成します
        /// </summary>
        /// <param name="memorySize"></param>
        public Interpreter(int memorySize)
        {
            context = new Context(memorySize);
        }

        /// <summary>
        /// 初期化します
        /// </summary>
        /// <returns></returns>
        public Interpreter Init()
        {
            context.Init();
            return this;
        }

        /// <summary>
        /// プログラムを読み込みます
        /// </summary>
        /// <param name="originalCode"></param>
        /// <returns></returns>
        public Interpreter Load(string originalCode)
        {
            context.SetCommands(originalCode);
            return this;
        }

        /// <summary>
        /// コードを実行します
        /// </summary>
        /// <returns>実行結果</returns>
        public IEnumerable<char> Execute(string input)
        {
            context.SetInput(input);
            while (!context.Commands.IsEnd)
            {
                var yieldResult = context.Commands.ExecuteStep(context);
                if (yieldResult.HasValue)
                {
                    yield return yieldResult.Value;
                }
            }
        }
    }
}
