﻿using System.Configuration;
using BrainfuckCore.Exception;

namespace BrainfuckCore
{
    /// <summary>
    /// Brainfuck インタプリタが使用する仮想メモリ
    /// </summary>
    internal class Memory
    {
        /// <summary>
        /// メモリの最大サイズ
        /// </summary>
        private static readonly int MaxMemorySize = int.Parse(ConfigurationManager.AppSettings["MaxMemorySize"]);

        /// <summary>
        /// メモリのポインタ
        /// </summary>
        private int _pointer;

        /// <summary>
        /// メモリの実体
        /// </summary>
        private readonly byte[] _memory;

        /// <summary>
        /// メモリサイズ
        /// </summary>
        public int MemorySize => _memory.Length;

        /// <summary>
        /// 現在のポインタがさしている値
        /// </summary>
        public byte CurrentValue
        {
            get { return _memory[_pointer]; }
            set { _memory[_pointer] = value; }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="memorySize">メモリサイズ</param>
        public Memory(int memorySize)
        {
            try
            {
                ValidateMemorySize(memorySize);
                _pointer = 0;
                _memory = new byte[memorySize];
            }
            catch (InvalidMemorySizeException)
            {
                throw;
            }
        }

        /// <summary>
        /// メモリを初期化します
        /// </summary>
        public void Init()
        {
            _pointer = 0;
            _memory.Initialize();
        }

        /// <summary>
        /// ポインタを一つ進めます
        /// </summary>
        public void MoveNext()
        {
            MoveIndex(_pointer + 1);
        }

        /// <summary>
        /// ポインタを一つ戻します
        /// </summary>
        public void MovePrevious()
        {
            MoveIndex(_pointer - 1);
        }

        /// <summary>
        /// ポインタが指す値をインクリメントします
        /// </summary>
        public void Increment()
        {
            CurrentValue++;
        }

        /// <summary>
        /// ポインタがさす値をデクリメントします
        /// </summary>
        public void Decrement()
        {
            CurrentValue--;
        }

        /// <summary>
        /// ポインタを移動させます
        /// </summary>
        /// <param name="destination"></param>
        private void MoveIndex(int destination)
        {
            try
            {
                ValidateIndex(destination);
                _pointer = destination;
            }
            catch (OutOfMemoryException)
            {
                throw;
            }
        }

        #region Validator
        /// <summary>
        /// 指定したメモリサイズが有効かどうか
        /// </summary>
        /// <param name="memorySize"></param>
        /// <returns></returns>
        private void ValidateMemorySize(int memorySize)
        {
            if (memorySize <= 0 || memorySize > MaxMemorySize)
            {
                throw new InvalidMemorySizeException(memorySize);
            }
        }

        /// <summary>
        /// 指定したインデックスが有効かどうか検証します
        /// </summary>
        /// <param name="index"></param>
        private void ValidateIndex(int index)
        {
            if (index >= 0 && index < MemorySize) return;
            throw new OutOfMemoryException(this.MemorySize, index);
        }
        #endregion
    }
}
