﻿namespace BrainfuckCore.Command
{
    internal sealed class MoveNext: Command
    {
        private const char CharExp = '>';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            context.Memory.MoveNext();
            return null;
        }
    }
}
