﻿namespace BrainfuckCore.Command
{
    internal sealed class GetChar: Command
    {
        private const char CharExp = '.';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            return (char)context.Memory.CurrentValue;
        }
    }
}
