﻿namespace BrainfuckCore.Command
{
    /// <summary>
    /// Brainfuckのコマンドを表します
    /// </summary>
    internal abstract class Command
    {
        /// <summary>
        /// コマンドの文字表記を取得します
        /// </summary>
        /// <returns></returns>
        protected abstract char ToChar();

        /// <summary>
        /// 与えられた文字がこのコマンドの文字列表記と一致するかどうか
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public bool Equals(char c)
        {
            return this.ToChar() == c;
        }

        /// <summary>
        /// コマンドを実行します。
        /// </summary>
        /// <param name="context">コンテキスト</param>
        /// <returns></returns>
        public abstract char? Execute(Context context);
    }
}
