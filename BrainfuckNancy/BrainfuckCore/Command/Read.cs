﻿namespace BrainfuckCore.Command
{
    internal sealed class Read: Command
    {
        private const char CharExp = ',';

        protected override char ToChar()
        {
            return CharExp;
        }

        public override char? Execute(Context context)
        {
            context.Memory.CurrentValue = context.Inputs.Read();
            return null;
        }
    }
}
