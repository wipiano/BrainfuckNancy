﻿#define DEBUG
using System;
using Nancy;
using Nancy.ModelBinding;
using BrainfuckNancy.Extensions;
using BrainfuckNancy.Models;
using BrainfuckCore;
using BrainfuckCore.Exception;

namespace BrainfuckNancy.Modules
{
    public class Module : NancyModule
    {
        public Module()
        {
            Get["/"] = parameters => View["index"];

            Post["/exec"] = _ =>
            {
                const string success = "Success.";

                var request = this.Bind<ExecRequest>();
                var result = new ExecResult();

                try
                {
                    result
                        .SetOutput(
                            new Interpreter(request.MemorySize)
                                .Load(request.Code)
                                .Execute(request.Input))
                        .SetResult(success);
                }
                catch (BrainfuckException e)
                {
                    result
                        .SetOutput(e.ToString())
                        .SetResult(e.ToShortString());
                }
#if DEBUG
                catch (Exception e)
                {
                    result.SetOutput(e.ToString());
                }
#endif

                return Response.AsJson<ExecResult>(result);
            };
        }
    }
}
