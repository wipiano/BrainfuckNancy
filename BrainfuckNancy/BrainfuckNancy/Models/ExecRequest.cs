﻿namespace BrainfuckNancy.Models
{
    /// <summary>
    /// 実行リクエスト
    /// </summary>
    class ExecRequest
    {
        /// <summary>
        /// メモリサイズ
        /// </summary>
        public int MemorySize { get; set; }

        /// <summary>
        /// ソースコード
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 入力
        /// </summary>
        public string Input { get; set; }
    }
}
