﻿namespace BrainfuckNancy.Models
{
    /// <summary>
    /// 実行結果
    /// </summary>
    class ExecResult
    {
        /// <summary>
        /// 結果の概要
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// インタプリタの出力
        /// </summary>
        public string Output { get; set; }
    }
}
