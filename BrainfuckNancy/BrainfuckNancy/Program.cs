﻿using System;
using System.Configuration;
using Nancy.Hosting.Self;

namespace BrainfuckNancy
{
    class Program
    {
        static void Main(string[] args)
        {
            var uri = new Uri(ConfigurationManager.AppSettings["ListenUri"]);

            using (var host = new NancyHost(uri))
            {
                host.Start();

                Console.WriteLine("BrainfuckNancy is running on " + uri);
                while (Console.ReadLine() != "exit")
                {
                }
            }
        }
    }
}
